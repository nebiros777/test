#include "stdafx.h"
#include "svga/svga.h"
#include <cmath>

//This function update full screen from scrptr. The array should be at least sv_height*scrpitch bytes size;
void w32_update_screen(void *scrptr,unsigned scrpitch);

//If this variable sets to true - game will quit

extern bool game_quited;

// these variables provide access to joystick and joystick buttons
// In this version joystick is simulated on Arrows and Z X buttons

// [0]-X axis (-501 - left; 501 - right)
// [1]-Y axis (-501 - left; 501 - right)
extern int gAxis[2];
//0 - not pressed; 1 - pressed
extern int gButtons[6];

//sv_width and sv_height variables are width and height of screen
extern unsigned int sv_width,sv_height;

//These functions called from another thread, when a button is pressed or released
void win32_key_down(unsigned k){
  if(k==VK_F1) game_quited=true;
}
void win32_key_up(unsigned){}

//This is default fullscreen shadow buffer. You can use it if you want to.
static unsigned *shadow_buf=NULL;

//Platform variables
int start_x_platform;
int start_y_platform;
bool WasPressed = false;

//Circle variables
int center_coord_x;
int center_coord_y;
bool GameStarted = false;
int Curcle_moving_speed_X = 8;
int Curcle_moving_speed_Y = 8;

//Cube variables


class Circle {
public:
	int radius = 20;
	int center_coord_x;
	int center_coord_y;
	int moving_speed = 2;

	Circle(int center_coord_x, int center_coord_y) : center_coord_x(center_coord_x), center_coord_y(center_coord_y) {}

	void Draw_Circle() {
		if (!shadow_buf)return;
		for (int i = 0; i < sv_width; i++) {
			for (int j = 0; j < sv_height; j++) {
				if (pow(i - center_coord_x, 2) + pow(j - center_coord_y, 2) <= pow(radius, 2)) {
					unsigned int pixel_index = j * sv_width + i;
					unsigned int R = 255;
					unsigned int G = 0;
					unsigned int B = 0;
					unsigned int A = 0;
					unsigned int color = A << 24 | R << 16 | G << 8 | B;
					shadow_buf[pixel_index] = color;

				}
			}
		}
	}

	
};

class Platform {
public:
	int start_x;
	int start_y;
	
	int speed_x = 5;

	Platform(int start_x, int start_y) : start_x(start_x), start_y(start_y) {}

	void Draw_Reactangle() {
		int end_x = start_x + 100;
		int end_y = start_y + 25;

		if (!shadow_buf)return;
		for (int i = 0; i < sv_width; i++) {
			for (int j = 0; j < sv_height; j++) {
				if (i > start_x && j > start_y && i <= end_x && j <= end_y) {
					unsigned int pixel_index = j * sv_width + i;
					unsigned int R = 255;
					unsigned int G = 255;
					unsigned int B = 0;
					unsigned int A = 0;
					unsigned int color = A << 24 | R << 16 | G << 8 | B;
					shadow_buf[pixel_index] = color;
				}
			}
		}

	}

	
		
};

class Cube {
public:
	int start_x;
	int start_y;
	bool was_hited = false;

	Cube(int start_x,int start_y) : start_x(start_x), start_y(start_y) {}

	void Draw_Cube() {

		int end_x = start_x + 35;
		int end_y = start_y + 35;

		if (!shadow_buf)return;
		for (int i = 0; i < sv_width; i++) {
			for (int j = 0; j < sv_height; j++) {
				if (i > start_x+5 && j > start_y+5 && i <= end_x && j <= end_y) {
					unsigned int pixel_index = j * sv_width + i;
					unsigned int R = 255;
					unsigned int G = 0;
					unsigned int B = 255;
					unsigned int A = 0;
					unsigned int color = A << 24 | R << 16 | G << 8 | B;
					shadow_buf[pixel_index] = color;
				}
			}
		}

	}
};
	void Draw_Hit(Cube* Cube) {
		
		int end_x = Cube->start_x + 40;
		int end_y = Cube->start_y + 40;
		Cube->was_hited = true;
		if (!shadow_buf)return;
		for (int i = 0; i < sv_width; i++) {
			for (int j = 0; j < sv_height; j++) {
				if (i > Cube->start_x + 5 && j > Cube->start_y + 5 && i <= end_x - 5 && j <= end_y - 5) {
					unsigned int pixel_index = j * sv_width + i;
					unsigned int R = 0;
					unsigned int G = 0;
					unsigned int B = 255;
					unsigned int A = 0;
					unsigned int color = A << 24 | R << 16 | G << 8 | B;
					shadow_buf[pixel_index] = color;
					
				}
			}
		}
	}







void init_game() {
	shadow_buf = new unsigned[sv_width * sv_height];
}

void close_game() {
	if (shadow_buf) delete shadow_buf;
	shadow_buf = NULL;
	WasPressed = false;
	GameStarted = false;
}

Cube* Cubes_Array[20][3];
//Platform* Ploschadka = NULL;

//draw the game to screen
void draw_game(){
  if(!shadow_buf)return;
  memset(shadow_buf,0,sv_width*sv_height*4);
  
  //here you should draw anything you want in to shadow buffer. (0 0) is left top corner



  //Building Cubes
  // �������������� ���������� �������
  if (!GameStarted) {
	  const unsigned int AmmointCubesInX = (sv_width - 5) / 40;
	  int StartX = 5;
	  for (int i = 0; i < AmmointCubesInX; i++) {
		  int StartY = 35;
		  for (int j = 0; j < 3; j++) {

			  Cubes_Array[i][j] = new Cube(StartX, StartY);
			  Cubes_Array[i][j]->Draw_Cube();
			  StartY += 40;
		  }
		  StartX += 40;
	  }
  }
  // ���������� ������� ��� ��������
  else {
	  for (int CubeIndexI = 0; CubeIndexI < (sv_width - 5) / 40; CubeIndexI++) {
		  for (int CubeIndexJ = 0; CubeIndexJ < 3; CubeIndexJ++) {
			  if (Cubes_Array[CubeIndexI][CubeIndexJ]->was_hited == false) {
				  Cubes_Array[CubeIndexI][CubeIndexJ]-> Draw_Cube();
			  }
		  }
	  }
  }



  // Platform logic

  if (!WasPressed) {
	  start_x_platform = sv_width / 2 - 50;
	  start_y_platform = sv_height - 50;
  }
  
 
  Platform* Ploschadka = new Platform(start_x_platform, start_y_platform); 
  //if (!GameStarted) {
  //	  Ploschadka = new Platform(start_x_platform, start_y_platform);
  // }
  Ploschadka->Draw_Reactangle();
  if (((gButtons[0] == 1) || (gButtons[2] == 1) || (gButtons[4] == 1)) && (start_x_platform > 0)) {
	  start_x_platform -= 8;
	  WasPressed = true;
  }
  else if (((gButtons[1] == 1) || (gButtons[3] == 1) || (gButtons[5] == 1)) && (start_x_platform + 100 < sv_width)) {
	  start_x_platform += 8;
	  WasPressed = true;
  }
 

  //Curcle logic
  //��������� ����� � ������ ����
  if (!GameStarted) {
	  center_coord_x = sv_width / 2;
	  center_coord_y = sv_height - 71;
	  GameStarted = true;
  }

  Circle* Krug = new Circle(center_coord_x, center_coord_y);
  Krug->Draw_Circle();

  // ������ �� ������ ����
  if ((center_coord_x + 20 >= sv_width) || (center_coord_x - 20 <= 0)) {
	  Curcle_moving_speed_X *= -1;
  }
  if ((center_coord_y - 35 <= 0)) {
	  Curcle_moving_speed_Y *= -1;
  }
  //Restart
  if (center_coord_y + 20 >= sv_height) {
	  close_game();
	  init_game();
  }

  //  ������ �� ��������
  if (center_coord_x + 20 >= start_x_platform && center_coord_x + 20 <= start_x_platform + 100 && center_coord_y + 20 >= start_y_platform) {
	  Curcle_moving_speed_Y *= -1;
  }
  // else if(center_coord_y + 20 >= start_y_platform && center_coord_y + 20 <= start_y_platform + 25 && ((center_coord_x + 20 >= start_x_platform) || (center_coord_x + 20 <= start_x_platform+100))){
  //	  Curcle_moving_speed_X *= -1;
  //}

  // ����������� ������
  for (int CubeIndexI = 0; CubeIndexI < (sv_width - 5) / 40; CubeIndexI++) {
	  for (int CubeIndexJ = 0; CubeIndexJ < 3; CubeIndexJ++) {
		  for (int X = center_coord_x - 20; X < center_coord_x + 20; X++) {
			  for (int Y = center_coord_y - 20; Y < center_coord_y + 20; Y++) {
				  if (X >= Cubes_Array[CubeIndexI][CubeIndexJ]->start_x + 5 && X <= Cubes_Array[CubeIndexI][CubeIndexJ]->start_x + 35 && Y >= Cubes_Array[CubeIndexI][CubeIndexJ]->start_y + 5 && Y <= Cubes_Array[CubeIndexI][CubeIndexJ]->start_y + 35&& Cubes_Array[CubeIndexI][CubeIndexJ]->was_hited==false) {
					  Draw_Hit(Cubes_Array[CubeIndexI][CubeIndexJ]);
					  Curcle_moving_speed_Y *= -1;
				  }
				  
			  }
		  }
	  }
  }


  //��������
  center_coord_x += Curcle_moving_speed_X;
  center_coord_y += Curcle_moving_speed_Y;
  

  
  w32_update_screen(shadow_buf,sv_width*4);
  
  
}

//act the game. dt - is time passed from previous act
void act_game(float dt){
	
}
